use strict;
use warnings;
use 5.010;

package Abalone::Network::Server;

use Carp;
use constant {
	ENDL => "\015\012",
};

use Abalone::Network::Connection;
our @ISA=('Abalone::Network::Connection');

sub new {
	my $class = shift;
	my %option = @_;
	my $self  = bless {
		interface => $option{'-interface'},
		port    => $option{'-port'},
		}, $class;
	return $self;
}

sub create_tcp_connection {
	my $self = shift;
	use Socket;
	use IO::Select;
	use IO::Socket::INET;
	my $lns = IO::Socket::INET->new(
		Listen    => 1,
		Type      => SOCK_STREAM,
		LocalPort => $self->{port},
		Proto     => 'tcp',
		) or die "Can not bind: $!";
	$self->{lns} = $lns;
	my $remote = $lns->accept;
	$self->{output_stream} = $remote;
	$self->{input_stream} = $remote;
}

1;
__END__
