use strict;
use warnings;

package Abalone::Network::Client;

use Carp;
use constant {
	ENDL => "\015\012",
};

use Abalone::Network::Connection;
our @ISA=('Abalone::Network::Connection');

sub new {
	my $class = shift;
	my %option = @_;
	my $self  = bless {
		address => $option{'-address'},
		port    => $option{'-port'},
		}, $class;
	return $self;
}

sub create_tcp_connection {
	my $self = shift;
	use Socket;
	use IO::Select;
	use IO::Socket::INET;
	my $remote = IO::Socket::INET->new(
		Type      => SOCK_STREAM,
		PeerPort  => $self->{port},
		Proto     => 'tcp',
		PeerAddr  => $self->{address},
		) or die "Can not bind: $!";
	$self->{output_stream} = $remote;
	$self->{input_stream} = $remote;
}

1;
__END__
