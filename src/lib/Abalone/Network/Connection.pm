use strict;
use warnings;
use 5.010;

package Abalone::Network::Connection;

use Carp;

use constant {
	ENDL => "\015\012",
};

sub send_click {
	my ($self, $tile) = @_;
	$self->send_message('click ', $tile);
}

sub send_message {
	use IO::Handle;
	my $self = shift;
	$self->{output_stream}->print(@_,ENDL);
	$self->{output_stream}->flush;
}

sub check_messages {
	my $self = shift;
	my @message;
	while (my $message = $self->read_message) {
		push @message, $message;
	}
	return @message;
}

sub read_message {
	use IO::Handle;
	my $self = shift;
	local $/ = ENDL;
	my $blocking = $self->{input_stream}->blocking(0);
	my $message = $self->{input_stream}->getline;
	chomp $message if $message;
	$self->{input_stream}->blocking($blocking);
	return $message;
}

1;
__END__
