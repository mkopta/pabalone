use strict;
use warnings;
use 5.010;

package Abalone::Model::Board;

use Carp;

our %map_tables;

sub new {
	my $class = shift;
	my $self = bless {}, $class;
	my $base = shift;
	$self->{base} = $base || 5;
	$base = $self->{base};
	my $lasttile = 3 * $base ** 2 - 3 * $base;
	$self->{tiles}[$lasttile] = 0;
	calculate_map($base);
	return $self;
}

sub parse {
	my $class = shift;
	my $input = shift;
	my $board;
	eval "$input";
	if ($@) {
		$! = $@;
		return;
	}
	calculate_map($board->{base});
	return $board;
}

sub calculate_map {
	my $base = shift;
	unless (exists $map_tables{$base}) {
		my $lasttile = 3 * $base ** 2 - 3 * $base;
		my $y = 0;
		my $x = $base - 1;
		my $tiles_on_line = $base;
		my $old_tiles = $base;
		for my $tile ( 0..$lasttile ) {
			$map_tables{$base}[$tile] = [$x, $y];
			$x += 2;
			unless ( --$tiles_on_line ) {
				$y++;
				$x = $base - 1 - $y;
				$x = abs($x);
				if ($y < $base) {
					$tiles_on_line = $old_tiles + 1;
				} else {
					$tiles_on_line = $old_tiles - 1;
				}
				$old_tiles = $tiles_on_line;
			}
		}
	}
}

sub dump_board {
	my $self = shift;
	use Data::Dumper;
	return Data::Dumper->Dump([$self], ['board']);
}

sub is_neighbour {
	my  ($self, $tile) = splice @_, 0, 2;
	for (@_) {
		for ($self->neighbours($_)) {
			return 1 if defined $_ and $_ == $tile;
		}
	}
	return 0;
}

sub neighbour_direction {
	my ($self, $tile0, $tile1) = @_;
	return undef unless $self->is_neighbour($tile0, $tile1);
	my @neighbour0 = $self->neighbours($tile0);
	for my $direction (0..$#neighbour0)  {
		return $direction if defined $neighbour0[$direction] and $neighbour0[$direction] == $tile1;
	}
}

sub get_tile {
	my $self = shift;
	my $tile_id = shift;
	return $self->{tiles}[$tile_id];
}

sub tile_id2coord {
	my ($self, $tile) = @_;
	return @{$map_tables{$self->{base}}[$tile]};
}

sub coord2tile_id {
	my ($self, $x, $y) = @_;
#FIXME: temporary solution
	for my $tile_id ( 0..$#{$self->{tiles}} ) {
		my ($xa, $ya) = $self->tile_id2coord($tile_id);
		if ( $x == $xa and $y == $ya) {
			return $tile_id;
		}
	}
	return undef;
}

sub neighbours {
	my ($self, $tile) = @_;
	my ($x, $y) = $self->tile_id2coord($tile);
	return (
		$self->coord2tile_id($x+2,$y),
		$self->coord2tile_id($x+1,$y-1),
		$self->coord2tile_id($x-1,$y-1),
		$self->coord2tile_id($x-2,$y),
		$self->coord2tile_id($x-1,$y+1),
		$self->coord2tile_id($x+1,$y+1),
		);
}

sub move_tiles {
	my ($self, $direction, $distance) = splice @_, 0, 3;
	my $sort;
	my ($dx, $dy);
	given ($direction) {
		when ( 0 ) {
			$dx = 2;
			$dy = 0;
			$sort = sub {
				($self->tile_id2coord($a))[0] <=> ($self->tile_id2coord($b))[0];
			};
		}
		when ( 1 ) {
			$dx = 1;
			$dy = -1;
			$sort = sub {
				($self->tile_id2coord($a))[0] <=> ($self->tile_id2coord($b))[0];
			};
		}
		when ( 2 ) {
			$dx = -1;
			$dy = -1;
			$sort = sub {
				($self->tile_id2coord($b))[0] <=> ($self->tile_id2coord($a))[0];
			};
		}
		when ( 3 ) {
			$dx = -2;
			$dy = 0;
			$sort = sub {
				($self->tile_id2coord($b))[0] <=> ($self->tile_id2coord($a))[0];
			};
		}
		when ( 4 ) {
			$dx = -1;
			$dy = 1;
			$sort = sub {
				($self->tile_id2coord($b))[0] <=> ($self->tile_id2coord($a))[0];
			};
		}
		when ( 5 ) {
			$dx = +1;
			$dy = 1;
			$sort = sub {
				($self->tile_id2coord($a))[0] <=> ($self->tile_id2coord($b))[0];
			};
		}
	}
	my @tile = reverse sort $sort @_;
	for my $old_tile (@tile) {
		my	($x, $y) = $self->tile_id2coord($old_tile);
		$x += $dx;
		$y += $dy;
		my $new_tile = $self->coord2tile_id($x,$y);
		if (defined $new_tile) {
			$self->{tiles}[$new_tile] = 
				$self->{tiles}[$old_tile];
		}
		$self->{tiles}[$old_tile] = 0;
	}
}

1;
__END__
