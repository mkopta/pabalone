use strict;
use warnings;
use 5.010;

use Carp;

package Abalone::Model;
use Abalone::Model::Board;

sub new {
	my $class = shift;
	my %option = @_;
	my $self = bless {
			selected_tiles => {},
			current_player => 1,
			local_player   => $option{-local_player} // 1,
			active_game    => 0,
			player         => {1=>{score=>0},2=>{score=>0}},
			network_game   => $option{-network_game},
		}, $class;
	return $self;
}

sub new_game {
	my $self = shift;
	my %option = @_;
	my $board = $option{-board};
	unless (defined $board) {
		local $/;
		my $def_board = <DATA>;
		$board = Abalone::Model::Board->parse($def_board);
	}
	$self->{board} = $board;
	$self->{network_game} = $option{-network_game} // 0;
	$self->{local_player} = $option{-local_player} // 1;
	$self->{active_game} = 1;
	$self->refresh_view;
}

sub new_desk {
	my ($self, $board) = @_;
	$self->{board} = $board;
}

sub tile {
	my ($self, $tile_id) = @_;
	return 0 unless defined $self->{board};
	return $self->{board}->get_tile($tile_id);
}

sub controller {
	my ($self, $controller) = @_;
	if (defined $controller) {
		$self->{controller} = $controller;
	}
	return $self->{controller};
}

sub add_view_listener {
	my ($self, $listener) = @_;
	push @{$self->{view_listeners}}, $listener;
}

sub refresh_view {
	my $self = shift;
	$_->model_updated for @{$self->{view_listeners}};
}

sub select_parser {
	my ($self, $tile) = @_;
	unless ( keys %{$self->{selected_tiles}} ) {
		if ($self->{board}->get_tile($tile) == $self->{current_player} ) {
			$self->{selected_tiles}{$tile} = 1;
			return 1;
		} else {
			return undef;
		}
	}
	if (exists $self->{selected_tiles}{$tile}) {
		delete $self->{selected_tiles}{$tile};
		return 1;
	}
	return undef
		unless $self->{board}->is_neighbour($tile,
		keys(%{$self->{selected_tiles}}));
	given ( () = keys(%{$self->{selected_tiles}}) ) {
		when ( 1 ) {
			given ( $self->{board}->get_tile($tile)) {
				when ( $self->{current_player} ) {
					$self->{selected_tiles}{$tile} = 1;
					return 1;
				}
				# empty tile
				when ( 0 ) {
					my $direction = $self->{board}->neighbour_direction(keys(%{$self->{selected_tiles}}), $tile);
					$self->{board}->move_tiles($direction,1,keys(%{$self->{selected_tiles}}));
					%{$self->{selected_tiles}} = ();
					$self->switch_player;
					return 1;
				}
				default {
					return undef;
				}
			}
		}
		when ( 2 ) {
			if ($self->{board}->get_tile($tile) == $self->{current_player}) {
				my @selected = keys %{$self->{selected_tiles}};
				my $dir1 = $self->{board}->neighbour_direction(@selected);
				my $dir = $self->{board}->neighbour_direction($tile, $selected[0])
					// $self->{board}->neighbour_direction($tile, $selected[1]);
				if ( ($dir1 % 3) == ($dir % 3) ) {
					$self->{selected_tiles}{$tile} = 1;
					return 1;
				} else {
					return undef;
				}
			} elsif ($self->{board}->get_tile($tile) == 0) {
				my @selected = keys %{$self->{selected_tiles}};
				my $dir1 = $self->{board}->neighbour_direction(@selected);
				my $dir = $self->{board}->neighbour_direction($tile, $selected[0])
					// $self->{board}->neighbour_direction($tile, $selected[1]);
					if ( ($dir1 % 3) == ($dir % 3) ) {
						$self->{board}->move_tiles( ($dir + 3)%6, 1, keys(%{$self->{selected_tiles}}));
						%{$self->{selected_tiles}} = ();
						$self->switch_player;
						return 1;
					} else {
						#side move
						my $is_neighbour0 = $self->{board}->is_neighbour($tile, $selected[0]);
						my $is_neighbour1 = $self->{board}->is_neighbour($tile, $selected[1]);
						if ( $is_neighbour0 xor $is_neighbour1 ) {
							$dir = $self->{board}->neighbour_direction($is_neighbour1?$selected[1]:$selected[0], $tile);
							my ($next_tile) = ($self->{board}->neighbours($is_neighbour1?$selected[0]:$selected[1]))[$dir];
							if ($self->{board}->get_tile($next_tile) == 0) {
								$self->{board}->move_tiles($dir,1,keys(%{$self->{selected_tiles}}));
								%{$self->{selected_tiles}} = ();
								$self->switch_player;
								return 1;
							} else {
								return undef;
							}
						} else {
							return undef;
						}
					}
			} else {
				my @selected = keys %{$self->{selected_tiles}};
				my $dir1 = $self->{board}->neighbour_direction(@selected);
				my $dir = $self->{board}->neighbour_direction($tile, $selected[0])
					// $self->{board}->neighbour_direction($tile, $selected[1]);
					if ( ($dir1 % 3) == ($dir % 3) ) {
						$dir += 3;
						$dir %= 6;
						my ($behind_tile) = ($self->{board}->neighbours($tile))[$dir];
						if (defined $behind_tile) {
							# push
							if ($self->{board}->get_tile($behind_tile) == 0) {
								$self->{board}->move_tiles($dir,1,@selected, $tile);
								%{$self->{selected_tiles}} = ();
								$self->switch_player;
								return 1;
							} else {
								return undef;
							}
						} else {
							# push out
							# moving $tile is not necessary
							$self->{board}->move_tiles($dir,1,@selected);
							%{$self->{selected_tiles}} = ();
							$self->switch_player;
							$self->{player}{$self->{current_player}}{score}++;
							$self->check_winner;
							return 1;
						}
					} else  {
						return undef;
					}
			}
		}
		when ( 3 ) {
			given ($self->{board}->get_tile($tile)) {
				when ( $self->{current_player} ) {
					# selecting
					return undef;
				}
				when ( 0 ) {
					# moving
					my @selected = sort {$a<=>$b} keys(%{$self->{selected_tiles}});
					if ($self->{board}->is_neighbour($selected[1],$tile)) {
						return undef;
					}
					my $dir1 = $self->{board}->neighbour_direction($selected[0], $selected[1]);
					my $is_neighbour0 = $self->{board}->is_neighbour($selected[0], $tile);
					my $dir = $self->{board}->neighbour_direction($selected[$is_neighbour0?0:2], $tile);
					if ( ($dir % 3) == ($dir1 % 3) ) {
						# straight move
						$self->{board}->move_tiles($dir, 1, @selected);
						%{$self->{selected_tiles}} = ();
						$self->switch_player;
						return 1;
					} else {
						# side move
						my ($mid_tile) = ($self->{board}->neighbours($selected[1]))[$dir];
						my ($side_tile) = ($self->{board}->neighbours($selected[$is_neighbour0?2:0]))[$dir];
						if ( $self->{board}->get_tile($side_tile) || $self->{board}->get_tile($mid_tile) ) {
							return undef;
						}
						$self->{board}->move_tiles($dir,1,@selected);
						%{$self->{selected_tiles}} = ();
						$self->switch_player;
						return 1;
					}
				}
				default {
					# pushing
					my @selected = sort {$a<=>$b} keys(%{$self->{selected_tiles}});
					if ($self->{board}->is_neighbour($tile,$selected[1])) {
						return undef;
					}
					my $dir1 = $self->{board}->neighbour_direction(@selected[0..1]);
					my $is_neighbour0 = $self->{board}->is_neighbour($selected[0], $tile);
					my $dir = $self->{board}->neighbour_direction($selected[$is_neighbour0?0:2], $tile);
					unless ( ($dir % 3) == ($dir1 % 3) ) {
						return undef;
					}
					my ($behind_tile) = ($self->{board}->neighbours($tile))[$dir];
					unless (defined $behind_tile) {
						# push out one
						$self->{board}->move_tiles($dir,1,@selected);
						%{$self->{selected_tiles}} = ();
						$self->switch_player;
						$self->{player}{$self->{current_player}}{score}++;
						$self->check_winner;
						return 1;
					}
					if ($self->{board}->get_tile($behind_tile) == $self->{current_player}) {
						return undef;
					}
					if ($self->{board}->get_tile($behind_tile) == 0) {
						# push one
						$self->{board}->move_tiles($dir, 1, @selected, $tile);
						%{$self->{selected_tiles}} = ();
						$self->switch_player;
						return 1;
					}
					my ($behind_behind_tile) = ($self->{board}->neighbours($behind_tile))[$dir];
					unless (defined $behind_behind_tile) {
						# push out two
						$self->{board}->move_tiles($dir,1,@selected,$tile);
						%{$self->{selected_tiles}} = ();
						$self->switch_player;
						$self->{player}{$self->{current_player}}{score}++;
						$self->check_winner;
						return 1;
					}
					given ($self->{board}->get_tile($behind_behind_tile)) {
						when ( $self->{current_player}) {
							return undef;
						}
						when ( 0 ) {
							# push two
							$self->{board}->move_tiles($dir, 1, @selected, $tile, $behind_tile);
							%{$self->{selected_tiles}} = ();
							$self->switch_player;
							return 1;
						}
						default {
							return undef;
						}
					}
				}
			}
		}
		default {
			die "non-fatal error in ".__FILE__." line ".__LINE__;
		}
	}
}

sub tile_select {
	my ($self, $tile) = @_;
	return undef unless $self->{current_player} == $self->{local_player};
	$self->select_parser($tile) if $self->{active_game};
	$self->refresh_view;
}

sub network_tile_select {
	my ($self, $tile) = @_;
	return undef if $self->{current_player} == $self->{local_player};
	$self->select_parser($tile) if $self->{active_game};
	$self->refresh_view;
}

sub switch_player {
	my $self = shift;
	$self->{current_player} ^= 0x03;
	unless ($self->{network_game}) {
		$self->{local_player} = $self->{current_player};
	}
	$self->{controller}->switched_player_action;
}

sub selected_tiles {
	my ($self) = @_;
	return %{$self->{selected_tiles}};
}

sub check_winner {
	my $self = shift;
	if ($self->{player}{1}{score} >= 6 or
		$self->{player}{2}{score} >= 6) {
		$self->{active_game} = 0;
	}
}

sub winner {
	my $self = shift;
	if ($self->{active_game}) {
		return undef;
	}
	return ($self->{player}{1}{score} < $self->{player}{2}{score})?1:2;
}

sub game_running {
	my $self = shift;
	return $self->{active_game};
}

sub score {
	my ($self, $player) = @_;
	return $self->{player}{$player}{score};
}

sub playing_player {
	my $self = shift;
	return $self->{current_player};
}

sub local_player {
	my $self = shift;
	return $self->{current_player} == $self->{local_player};
}

sub is_network_game {
	my $self = shift;
	return $self->{network_game};
}

1;
__DATA__
$board = bless( {
                  'base' => 5,
                  'tiles' => [ qw/ 1 1 1 1 1
                                  1 1 1 1 1 1
                                 0 0 1 1 1 0 0
                                0 0 0 0 0 0 0 0
                               0 0 0 0 0 0 0 0 0
                                0 0 0 0 0 0 0 0
                                 0 0 2 2 2 0 0
                                  2 2 2 2 2 2
                                   2 2 2 2 2/ ]
                }, 'Abalone::Model::Board' );
__END__
