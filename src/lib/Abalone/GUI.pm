use strict;
use warnings;

package Abalone::GUI;
use Carp;
use Tk;

our %borg = ();
our %constant = (
	PI => 3.1416,
	TILE_HEIGHT_FACTOR => 0.866,
	TILE_WIDTH_FACTOR => 0.5,
	TILE_BASE => 5,
);
$constant{TILE_COUNT} = 3 * $constant{TILE_BASE} ** 2 - 3 * $constant{TILE_BASE} + 1;
$constant{TILE_ROWS} = 2 * $constant{TILE_BASE} - 1;
$constant{TILE_COLS} = 4 * $constant{TILE_BASE} - 3;

sub new {
	my $class = shift;
	init() unless keys %borg;
	return bless \%borg, $class;
}

sub main_loop {
	$borg{controller}->new_game_action;
	MainLoop();
}

sub init {
	use List::Util qw/min/;
	$borg{widget}{top} = MainWindow->new(-title => 'Abalone desk - Perl/Tk');
	$borg{widget}{desk} = $borg{widget}{top}->Canvas(
		-width => 500,
		-height => 500,
		-border => 1,
		-relief => 'ridge',
		);
	$borg{widget}{desk}->pack(-side => 'top', -fill => 'both', -expand => 'both');
	$borg{widget}{statusbarpanel} = $borg{widget}{top}->Frame();
	$borg{widget}{statusbarleft} = $borg{widget}{statusbarpanel}->Label()->pack(-side => 'left', -fill => 'x', -expand => 'x');
	$borg{widget}{statusbarpanel}->pack(-side => 'bottom', );
	my ($top, $botom, $left, $right) = (
		5,  
		$borg{widget}{desk}->reqheight -5, 
		5, 
		$borg{widget}{desk}->reqwidth -5);

	my $center_x = int(($left+$right)/2);
	my $center_y = int(($top+$botom)/2);
	my $radius = min($center_x - $left, $center_y - $top);
	my $tile_interval = int($radius/$constant{TILE_BASE});
	my $tile_size = $tile_interval*0.7;
	my @border_points = map { (@$_) }hexagonCorners($center_x, $center_y, $radius);
	push @border_points, @border_points[0..1];
	$borg{drawing}{outer_border} = $borg{widget}{desk}->create(
		'line',
		@border_points,
		-tags => qw/border/,
		-width => 4,
		);
# draw tiles
	my %border_tile;
	@border_tile{qw/middle_right top_right top_left middle_left bottom_left bottom_right/} = 
		hexagonCorners($center_x,$center_y,$radius - $tile_interval);
	my @left_border_tile = (
		$border_tile{top_left},
		segment_line($border_tile{top_left}, $border_tile{middle_left}, $constant{TILE_BASE}),
		$border_tile{middle_left},
		segment_line($border_tile{middle_left}, $border_tile{bottom_left}, $constant{TILE_BASE}),
		$border_tile{bottom_left},
		);
	my @right_border_tile = (
		$border_tile{top_right},
		segment_line($border_tile{top_right}, $border_tile{middle_right}, $constant{TILE_BASE}),
		$border_tile{middle_right},
		segment_line($border_tile{middle_right}, $border_tile{bottom_right}, $constant{TILE_BASE}),
		$border_tile{bottom_right},
		);
	my @tile;
	for my $i ( 0..$#left_border_tile ) {
		my $segment_count = $constant{TILE_BASE} + (($i < $constant{TILE_BASE})?$i:($constant{TILE_BASE}-$i+3));
		push @tile, $left_border_tile[$i];
		push @tile, segment_line($left_border_tile[$i],$right_border_tile[$i],$segment_count);
		push @tile, $right_border_tile[$i];
	}
	for my $i ( 0..$#tile) {
		$borg{drawing}{"tile_$i"} = $borg{widget}{desk}->create(
			'oval',
			int($tile[$i][0]-$tile_size/2), int($tile[$i][1]-$tile_size/2),
			int($tile[$i][0]+$tile_size/2), int($tile[$i][1]+$tile_size/2),
			-tags => ['empty_tile', "tile_$i"],
			-width => 2,
			-fill => 'gray',
			);
		$borg{widget}{desk}->bind("tile_$i", '<Button-1>' => [\&_tile_mouse_click, $i]);
	}
}

sub hexagonCorners {
	my ($top, $botom, $left, $right, $center_x, $center_y, $radius);
	if (@_ == 4) {
		($top, $botom, $left, $right) = @_;
		$center_x = int(($left+$right)/2);
		$center_y = int(($top+$botom)/2);
		use List::Util qw/min/;
		$radius = min($center_x - $left, $center_y - $top);
	} elsif (@_ == 3) {
		($center_x, $center_y, $radius) = @_;
	} else {
		croak 'Wrong usage of subrutine hexagonCorners';
	}
	my @points;
	my $angle = 0;
	while ( $angle < 6 ) {
		push @points, [];
		$points[-1][0] = int($center_x + cos($angle * $constant{PI}/3) * $radius);
		$points[-1][1] = int($center_y - sin($angle * $constant{PI}/3) * $radius);
		$angle += 1;
	}
	return @points;
}

sub segment_line {
	my ($start_point, $end_point, $segment_count) = @_;
	$segment_count -= 1;
	croak 'Wrong use of subrutine segment_line' unless ref($start_point) eq 'ARRAY' and
		ref($end_point) eq 'ARRAY' and defined $segment_count;
	my @point;
	my $delta_point = [($end_point->[0]-$start_point->[0])/$segment_count,
		($end_point->[1]-$start_point->[1])/$segment_count];
	for my $i (1..$segment_count-1) {
		push @point, [
			int($start_point->[0] + $delta_point->[0] * $i),
			int($start_point->[1] + $delta_point->[1] * $i)];
	}
	return @point;
}

sub paint_tiles {
	my %selected = $borg{model}->selected_tiles;
	for my $tile_id (0..($constant{TILE_COUNT} - 1)) {
		my $tile = $borg{model}->tile($tile_id);
		if ($tile == 1) {
			$borg{widget}{desk}->itemconfigure($borg{drawing}{"tile_$tile_id"}, -fill => 'black');
		} elsif ($tile == 2) {
			$borg{widget}{desk}->itemconfigure($borg{drawing}{"tile_$tile_id"}, -fill => 'white');
		} else {
			$borg{widget}{desk}->itemconfigure($borg{drawing}{"tile_$tile_id"}, -fill => 'gray');
		}
		if (exists $selected{$tile_id}) {
			$borg{widget}{desk}->itemconfigure($borg{drawing}{"tile_$tile_id"}, -outline => 'yellow');
		} else {
			$borg{widget}{desk}->itemconfigure($borg{drawing}{"tile_$tile_id"}, -outline => 'black');
		}
	}
}

sub model {
	my ($self, $model) = @_;
	if (defined $model) {
		$borg{model} = $model;
		$model->add_view_listener($self);
		$self->model_updated;
	}
	return $borg{model};
}

sub controller {
	my ($self, $controller) = @_;
	if (defined $controller) {
		$borg{controller} = $controller;
	}
	return $borg{controller};
}

sub _tile_mouse_click {
	my ($sender, $tile) = @_;
	$borg{controller}->tile_click_action($tile);
}

sub model_updated {
	my ($self) = @_;
	$self->paint_tiles;
	unless ($borg{model}->game_running) {
		$borg{widget}{desk}->configure(-state => 'disabled');
		$borg{widget}{statusbarleft}->configure(-text => "Game ended, winner is " . (($borg{model}->winner==1)?'black':'white'));
	} else {
		$borg{widget}{desk}->configure(-state => 'normal');
		my $status_text = 'Black : ' . 
		$borg{model}->score(1) . 
		' White : ' . 
		$borg{model}->score(2) . 
		' | now playing: ' . 
		(($borg{model}->playing_player == 1)?'black':'white');
		$borg{widget}{statusbarleft}->configure(-text => $status_text);
	}
}

sub network_timer {
	my ($self, $start) = @_;
	if ($start) {
		$borg{widget}{network_timer} = $borg{widget}{desk}->repeat(300, \&_network_timer_tick);
	} else {
		$borg{widget}{desk}->afterCancel($borg{widget}{network_timer});
	}
}

sub _network_timer_tick {
	my $sender = shift;
	$borg{controller}->network_timer_tick_action;
}


1;
__END__
