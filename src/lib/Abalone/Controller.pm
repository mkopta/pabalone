use strict;
use warnings;
use 5.010;

package Abalone::Controller;
use Carp;

sub new {
	my $class = shift;
	my $self = bless {}, $class;
	return $self;
}

sub view {
	my ($self, $view) = @_;
	if (defined $view) {
		$self->{view} = $view;
		$self->{view}->controller($self);
	}
	return $self->{view};
}

sub model {
	my ($self, $model) = @_;
	if (defined $model) {
		$self->{model} = $model;
		$model->controller($self);
	}
	return $self->{model};
}

sub network {
	my ($self, $network) = @_;
	if (defined $network) {
		$self->{network} = $network;
		$self->{network}->controller($self);
	}
	return $self->{network};
}

sub tile_click_action {
	my ($self, $tile) = @_;
	$self->{model}->tile_select($tile);
	$self->{network}->tile_select($tile) if defined $self->{network};
}

sub tile_network_click_action {
	my ($self, $tile) = @_;
	$self->{model}->network_tile_select($tile);
}

sub new_game_action {
	my $self = shift;
	if (defined $self->{network}) {
		if ($self->{network}->is_connected) {
			$self->{model}->new_game(-network_game => 1, 
				-local_player => ($self->{network}->is_server?1:2));
			$self->{view}->network_timer(1);
		} else {
			$self->{network}->create_connection;
		}
	} else {
		$self->{model}->new_game;
	}
}

sub switched_player_action {
	my $self = shift;
}

sub network_timer_tick_action {
	my $self = shift;
	$self->{network}->check_incoming_messages;
}

sub incoming_network_message_action {
	my ($self, $message) = @_;
	given ($message) {
		when ( m/^click (?<tile>\d+)$/ ) {
			$self->{model}->network_tile_select($+{tile});
		}
		default {
			# TODO: bad message
		}
	}
}

sub connection_established_action {
	my $self = shift;
	$self->{model}->new_game(-network_game => 1, 
		-local_player => ($self->{network}->is_server?1:2));
	$self->{view}->network_timer(1);
	$self->{model}->refresh_view;
}

1;
__END__
