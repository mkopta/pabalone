use strict;
use warnings;
use 5.010;

package Abalone::Network;

use Carp;

use Abalone::Network::Server;
use Abalone::Network::Client;
use Abalone::Network::Connection;

sub new {
	my $class = shift;
	my %option = @_;
	my  $self = bless {
		is_server => $option{'-listen'},
		port      => $option{'-port'},
		address   => $option{'-address'},
		interface => $option{'-interface'},
		}, $class;
	my $connection = ('Abalone::Network::' . ($self->{is_server}?'Server':'Client'))->new(@_);
	$self->{connection} = $connection;
	return $self;
}

sub controller {
	my ($self, $controller) = @_;
	if (defined $controller) {
		$self->{controller} = $controller;
	}
	return $self->{controller};
}

sub tile_select {
	my ($self, $tile) = @_;
	$self->{connection}->send_click($tile);
}

sub is_server {
	my $self = shift;
	return $self->{is_server};
}

sub check_incoming_messages {
	my $self = shift;
	my @message = $self->{connection}->check_messages;
	for my $message (@message) {
		given ($message) {
			default {
				$self->{controller}->incoming_network_message_action($message);
			}
		}
	}
}

sub create_connection {
	my $self = shift;
	$self->{connection}->create_tcp_connection;
	$self->{is_connected} = 1;
	$self->{controller}->connection_established_action;
}

sub is_connected {
	my $self = shift;
	return $self->{is_connected};
}

1;
__END__
